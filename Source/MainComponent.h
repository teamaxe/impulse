/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "MidiManager.h"
#include "AxiomComponents/PitchModComponent.h"
#include "AxiomComponents/SliderControlComponent.h"
#include "AxiomComponents/ProgButtonComponent.h"
#include "AxiomComponents/RotaryControlComponent.h"
#include "AxiomComponents/TransportButtonComponent.h"
#include "DrumPadComponent.h"

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainContentComponent   :  public Component,
                                public MidiKeyboardStateListener
{
public:
    //==============================================================================
    MainContentComponent (MidiManager& m);
    ~MainContentComponent();

    void paint (Graphics&) override;
    void resized() override;
    
    void handleNoteOn (MidiKeyboardState* source,
                       int midiChannel, int midiNoteNumber, float velocity) override;
    void handleNoteOff (MidiKeyboardState* source,
                        int midiChannel, int midiNoteNumber, float velocity) override;


private:
    TooltipWindow tooltipWindow;
    MidiManager& midiManager;
    
    MidiKeyboardState keyboardState;
    MidiKeyboardComponent keyboard;
    
    //
    PitchModComponent pitchModComponent;
    SliderControlComponent sliderControlComponent;
    ProgButtonComponent progButtonComponent;
    RotaryControlComponent rotaryControlComponent;
    TransportButtonComponent transportButtonComponent;
    DrumPadComponent drumPadComponent;
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
