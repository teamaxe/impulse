/*
  ==============================================================================

    MidiManager.h
    Created: 25 Nov 2016 12:58:41pm
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef MIDIMANAGER_H_INCLUDED
#define MIDIMANAGER_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

class MidiManager
{
    public:
    MidiManager() : midiOutput (MidiOutput::createNewDevice("Virtual Impulse\n"))
    {
        midiOutput->startBackgroundThread();
    }
    void sendMessage (MidiMessage& message)
    {
        midiOutput->sendBlockOfMessages (MidiBuffer (message), Time::getMillisecondCounter(), 44100);
    }
    
    private:
        ScopedPointer<MidiOutput> midiOutput;	//this will be our virtual midi input
};

#endif  // MIDIMANAGER_H_INCLUDED
